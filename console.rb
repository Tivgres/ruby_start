# frozen_string_literal: true

class Console
  def initialize
    show_lines
  end

  def show_lines
    (0..30).each do |i|
      if [10, 20, 25].include?(i)
        puts i.to_s + ': ' + 'ruby'
      else
        puts i.to_s + ': ' + '<3 ruby'
      end
    end
  end
end
