# frozen_string_literal: true

class Matrix
  def initialize
    draw_matrix
  end

  def draw_matrix
    puts 'Enter matrix size below:'
    matrix_size = gets.chomp.to_i
    (0..matrix_size).each do |i|
      (0..matrix_size).each do |j|
        if j == i
          print '1'
        else
          print '0'
         end
      end
      puts
    end
  end
end
