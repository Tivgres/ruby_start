# frozen_string_literal: true

class Arrays
  def initialize
    array_types
  end

  def array_types
    array = [
      [1, 2, 3, 4, '1'],
      %w[2 5 10],
      [111, 222, 333, 444],
      %w[i love ruby],
      { key: 'value' },
      [[['text', 100_000]]]
    ]
    puts get_sub_array(array, String)
  end

  def get_sub_array(array, type)
    temp_list = []
    array.each do |x|
      if x.is_a? type
        temp_list.push(x)
      else
        temp_list.push(get_sub_array(x, type)) if x.is_a? Array
      end
    end
    temp_list
  end
end
