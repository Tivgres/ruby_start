# frozen_string_literal: true

require_relative 'console'
require_relative 'drawer'
require_relative 'matrix'
require_relative 'recursive'
require_relative 'cases'
require_relative 'arrays'

class Main
  def initialize
    start_select
  end

  def start_select
    user_select = 999
    while user_select.positive?
      show_possible_select
      user_select = gets.chomp.to_i
      if (1..6).cover?(user_select)
        case user_select
        when 1
          Console.new
        when 2
          Drawer.new
        when 3
          Matrix.new
        when 4
          Recursive.new
        when 5
          Cases.new
        when 6
          Arrays.new
        else
          puts 'Wrong input, try again!'
        end
      else
        if user_select < 1
          puts 'Bye-bye'
        else
          puts 'Wrong input, try again!'
        end
      end
    end
  end

  def show_possible_select
    make_delimiter
    show_header
    show_list_available
    make_delimiter
  end

  def make_delimiter
    puts '=' * 30
  end

  def show_header
    puts 'Select a task for demonstration:'
  end

  def show_list_available
    puts [
      '1 - Work with console',
      '2 - Draw a circle',
      '3 - Draw a matrix',
      '4 - Recursive search',
      '5 - Work with cases',
      '6 - Work with arrays',
      'FOR EXIT - any number <= 0 || String'
    ]
  end
end

Main.new
