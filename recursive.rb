# frozen_string_literal: true

class Recursive
  def initialize
    hash = { key1: {}, key2: {}, key3: { key4: 'str', key5: 'str2', key6: { key7: { key8: 1, key9: [2] } } } }
    search hash, 'key9'
  end

  def search(temp_hash, needed_key)
    temp_hash.each_pair do |key, value|
      if key.to_s.eql? needed_key
        puts 'found next value: ' + value.to_s
      else
        search value, needed_key if value.is_a?(Hash)
      end
    end
  end
end
