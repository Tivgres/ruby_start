# frozen_string_literal: true

class Drawer
  def initialize
    draw_circle
  end

  def draw_circle
    puts 'Enter circle diameter below:'
    check_circle gets.chomp.to_i
  end

  def check_circle(circle_diameter)
    if circle_diameter >= 0
      draw circle_diameter
    else
      puts 'too smal for calculate'
     end
  end

  def draw(diameter)
    radius = diameter / 2
    (-radius..radius).each do |x|
      (-radius..radius).each do |y|
        print Math.sqrt(x**2 + y**2).round == radius ? '#' : '.'
      end
      puts
    end
  end
end
