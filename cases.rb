# frozen_string_literal: true

class Cases
  def initialize
    work_with_cases
  end

  def work_with_cases
    puts 'Write your string below:'
    temp_str = gets.chomp.to_s
    if temp_str.empty?
      puts 'Your string is empty, try again!'
      work_with_cases
    else
      show_possible_cases
      user_select = 0
      until [1, 2, 3].include?(user_select)
        puts 'Choose your new style:'
        user_select = gets.chomp.to_i
      end
      split_symbol = ' '
      case user_select
      when 1
        puts make_camel_case(temp_str.downcase.split(split_symbol))
      when 2
        puts temp_str.downcase.sub(split_symbol, '_')
      when 3
        puts temp_str.downcase.sub(split_symbol, '-')
      else
        puts 'Wrong input, try again in next time!'
      end
     end
  end

  def show_possible_cases
    puts [
      '1 - for CamelCase',
      '2 - for ruby_case_underscore',
      '3 - for css-case'
    ]
  end

  def make_camel_case(temp_words_array)
    temp_string = ''
    temp_words_array.each { |word| temp_string += word.capitalize!.to_s }
    temp_string
  end
end
